<?php

namespace App\Models\exercise;

use Illuminate\Database\Eloquent\Model;

class users extends Model
{
    protected $table = 'user';

    protected $primaryKey = 'user_id';
}
